export interface UserInfo {
    nombre: string;
    apellido: string;
    email: string;
    dni: string;
    alta: string;
    domicilio: string;
    
}